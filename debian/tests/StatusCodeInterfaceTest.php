<?php

declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;
use \Fig\Http\Message\StatusCodeInterface;

class StatusCodeInterfaceImpl implements StatusCodeInterface {

}

class StatusCodeInterfaceTest extends TestCase
{

    public function testImplementation(): void
    {
        $impl = new StatusCodeInterfaceImpl();

        // Informational 1xx
        $this->assertSame(100, StatusCodeInterfaceImpl::STATUS_CONTINUE);
        $this->assertSame(101, StatusCodeInterfaceImpl::STATUS_SWITCHING_PROTOCOLS);
        $this->assertSame(102, StatusCodeInterfaceImpl::STATUS_PROCESSING);
        $this->assertSame(103, StatusCodeInterfaceImpl::STATUS_EARLY_HINTS);
        // Successful 2xx
        $this->assertSame(200, StatusCodeInterfaceImpl::STATUS_OK);
        $this->assertSame(201, StatusCodeInterfaceImpl::STATUS_CREATED);
        $this->assertSame(202, StatusCodeInterfaceImpl::STATUS_ACCEPTED);
        $this->assertSame(203, StatusCodeInterfaceImpl::STATUS_NON_AUTHORITATIVE_INFORMATION);
        $this->assertSame(204, StatusCodeInterfaceImpl::STATUS_NO_CONTENT);
        $this->assertSame(205, StatusCodeInterfaceImpl::STATUS_RESET_CONTENT);
        $this->assertSame(206, StatusCodeInterfaceImpl::STATUS_PARTIAL_CONTENT);
        $this->assertSame(207, StatusCodeInterfaceImpl::STATUS_MULTI_STATUS);
        $this->assertSame(208, StatusCodeInterfaceImpl::STATUS_ALREADY_REPORTED);
        $this->assertSame(226, StatusCodeInterfaceImpl::STATUS_IM_USED);
        // Redirection 3xx
        $this->assertSame(300, StatusCodeInterfaceImpl::STATUS_MULTIPLE_CHOICES);
        $this->assertSame(301, StatusCodeInterfaceImpl::STATUS_MOVED_PERMANENTLY);
        $this->assertSame(302, StatusCodeInterfaceImpl::STATUS_FOUND);
        $this->assertSame(303, StatusCodeInterfaceImpl::STATUS_SEE_OTHER);
        $this->assertSame(304, StatusCodeInterfaceImpl::STATUS_NOT_MODIFIED);
        $this->assertSame(305, StatusCodeInterfaceImpl::STATUS_USE_PROXY);
        $this->assertSame(306, StatusCodeInterfaceImpl::STATUS_RESERVED);
        $this->assertSame(307, StatusCodeInterfaceImpl::STATUS_TEMPORARY_REDIRECT);
        $this->assertSame(308, StatusCodeInterfaceImpl::STATUS_PERMANENT_REDIRECT);
        // Client Errors 4xx
        $this->assertSame(400, StatusCodeInterfaceImpl::STATUS_BAD_REQUEST);
        $this->assertSame(401, StatusCodeInterfaceImpl::STATUS_UNAUTHORIZED);
        $this->assertSame(402, StatusCodeInterfaceImpl::STATUS_PAYMENT_REQUIRED);
        $this->assertSame(403, StatusCodeInterfaceImpl::STATUS_FORBIDDEN);
        $this->assertSame(404, StatusCodeInterfaceImpl::STATUS_NOT_FOUND);
        $this->assertSame(405, StatusCodeInterfaceImpl::STATUS_METHOD_NOT_ALLOWED);
        $this->assertSame(406, StatusCodeInterfaceImpl::STATUS_NOT_ACCEPTABLE);
        $this->assertSame(407, StatusCodeInterfaceImpl::STATUS_PROXY_AUTHENTICATION_REQUIRED);
        $this->assertSame(408, StatusCodeInterfaceImpl::STATUS_REQUEST_TIMEOUT);
        $this->assertSame(409, StatusCodeInterfaceImpl::STATUS_CONFLICT);
        $this->assertSame(410, StatusCodeInterfaceImpl::STATUS_GONE);
        $this->assertSame(411, StatusCodeInterfaceImpl::STATUS_LENGTH_REQUIRED);
        $this->assertSame(412, StatusCodeInterfaceImpl::STATUS_PRECONDITION_FAILED);
        $this->assertSame(413, StatusCodeInterfaceImpl::STATUS_PAYLOAD_TOO_LARGE);
        $this->assertSame(414, StatusCodeInterfaceImpl::STATUS_URI_TOO_LONG);
        $this->assertSame(415, StatusCodeInterfaceImpl::STATUS_UNSUPPORTED_MEDIA_TYPE);
        $this->assertSame(416, StatusCodeInterfaceImpl::STATUS_RANGE_NOT_SATISFIABLE);
        $this->assertSame(417, StatusCodeInterfaceImpl::STATUS_EXPECTATION_FAILED);
        $this->assertSame(418, StatusCodeInterfaceImpl::STATUS_IM_A_TEAPOT);
        $this->assertSame(421, StatusCodeInterfaceImpl::STATUS_MISDIRECTED_REQUEST);
        $this->assertSame(422, StatusCodeInterfaceImpl::STATUS_UNPROCESSABLE_ENTITY);
        $this->assertSame(423, StatusCodeInterfaceImpl::STATUS_LOCKED);
        $this->assertSame(424, StatusCodeInterfaceImpl::STATUS_FAILED_DEPENDENCY);
        $this->assertSame(425, StatusCodeInterfaceImpl::STATUS_TOO_EARLY);
        $this->assertSame(426, StatusCodeInterfaceImpl::STATUS_UPGRADE_REQUIRED);
        $this->assertSame(428, StatusCodeInterfaceImpl::STATUS_PRECONDITION_REQUIRED);
        $this->assertSame(429, StatusCodeInterfaceImpl::STATUS_TOO_MANY_REQUESTS);
        $this->assertSame(431, StatusCodeInterfaceImpl::STATUS_REQUEST_HEADER_FIELDS_TOO_LARGE);
        $this->assertSame(451, StatusCodeInterfaceImpl::STATUS_UNAVAILABLE_FOR_LEGAL_REASONS);
        // Server Errors 5xx
        $this->assertSame(500, StatusCodeInterfaceImpl::STATUS_INTERNAL_SERVER_ERROR);
        $this->assertSame(501, StatusCodeInterfaceImpl::STATUS_NOT_IMPLEMENTED);
        $this->assertSame(502, StatusCodeInterfaceImpl::STATUS_BAD_GATEWAY);
        $this->assertSame(503, StatusCodeInterfaceImpl::STATUS_SERVICE_UNAVAILABLE);
        $this->assertSame(504, StatusCodeInterfaceImpl::STATUS_GATEWAY_TIMEOUT);
        $this->assertSame(505, StatusCodeInterfaceImpl::STATUS_VERSION_NOT_SUPPORTED);
        $this->assertSame(506, StatusCodeInterfaceImpl::STATUS_VARIANT_ALSO_NEGOTIATES);
        $this->assertSame(507, StatusCodeInterfaceImpl::STATUS_INSUFFICIENT_STORAGE);
        $this->assertSame(508, StatusCodeInterfaceImpl::STATUS_LOOP_DETECTED);
        $this->assertSame(510, StatusCodeInterfaceImpl::STATUS_NOT_EXTENDED);
        $this->assertSame(511, StatusCodeInterfaceImpl::STATUS_NETWORK_AUTHENTICATION_REQUIRED);
    }
}
