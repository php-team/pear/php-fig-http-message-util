<?php

declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;
use \Fig\Http\Message\RequestMethodInterface;

class RequestMethodInterfaceImpl implements RequestMethodInterface {

}

class RequestMethodInterfaceTest extends TestCase
{

    public function testImplementation(): void
    {
        $impl = new RequestMethodInterfaceImpl();
        $this->assertSame('HEAD', RequestMethodInterfaceImpl::METHOD_HEAD);
        $this->assertSame('GET', RequestMethodInterfaceImpl::METHOD_GET);
        $this->assertSame('POST', RequestMethodInterfaceImpl::METHOD_POST);
        $this->assertSame('PUT', RequestMethodInterfaceImpl::METHOD_PUT);
        $this->assertSame('PATCH', RequestMethodInterfaceImpl::METHOD_PATCH);
        $this->assertSame('DELETE', RequestMethodInterfaceImpl::METHOD_DELETE);
        $this->assertSame('PURGE', RequestMethodInterfaceImpl::METHOD_PURGE);
        $this->assertSame('OPTIONS', RequestMethodInterfaceImpl::METHOD_OPTIONS);
        $this->assertSame('TRACE', RequestMethodInterfaceImpl::METHOD_TRACE);
        $this->assertSame('CONNECT', RequestMethodInterfaceImpl::METHOD_CONNECT);
    }
}
